<?php
session_start();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//echo $_POST['id'];
include 'connection.php';
//Confirm Editing
if(isset($_POST['btnUpdate'])){
    $newName = $_POST['productName'];
    $newDescription = $_POST['productDescription'];
    $newPrice = (int)$_POST['productPrice'];
    $newLink = $_POST['productLink'];
    $newImage = $_FILES['file']['name'];
    $newDBImage = time() . $newImage;//Store this name in DB
    $newImageSource = $_FILES['file']['tmp_name'];
    $ImageDestination = "ProductImages/" . $newDBImage;
    $pID = (int)$_POST['id'];
     if($newImage != NULL){
         if(move_uploaded_file($newImageSource, $ImageDestination)){
            $sql = "UPDATE products SET Product_image = '$newDBImage', Product_name = '$newName', Product_description = '$newDescription', Product_price = $newPrice, Product_video_link = '$newLink' where Product_ID = $pID";    
            $result = mysqli_query($con, $sql);
            if($result == FALSE){
                echo 'Error inserting into database.';
            }else{
                header("Location: products.php");
            }
        }
    }else{
        $sql = "UPDATE products SET Product_name = '$newName', Product_description = '$newDescription', Product_price = $newPrice, Product_video_link = '$newLink' where Product_ID = $pID;";    
        $result = mysqli_query($con, $sql);
        if($result == FALSE){
            echo 'Error inserting into database.';
        }else{
            header("Location: products.php");
        }
    }
     
}
//Confirm Deleting
else if(isset($_POST['btnConfirmDeleting'])){
    $pID = (int)$_POST['id'];
    $sql = "DELETE FROM products where Product_ID = $pID;";    
    $result = mysqli_query($con, $sql);
    if($result == FALSE){
        echo 'Error inserting into database.';
    }else{
         header("Location: products.php");
    }
}
//Cancel updating
else if(isset($_POST['btnCancelUpdate'])){
    header('Location: products.php');
}
//Cancel Deleting
else if(isset($_POST['btnCancelDeleting'])){
    header('Location: products.php');
}
//Deleting
else if(isset($_POST['delete'])){
    //It would great if you can make this popup modal(Sandip and Tania)
    ?>
<form action="?" method="post">
    <h4> Are you sure to permanently delete this product?</h4>
    <input type="hidden" name="id" value='<?php echo $_POST['id'];?>'/>
    <input type="submit" name="btnConfirmDeleting" value="Delete"/>
    <input type="submit" name="btnCancelDeleting" value="Cancel"/>
</form>
<?php
}
//Editing 
else if(isset($_POST['edit'])){
    //echo 'Edit Id' . $_POST['id'];
    //Edit here
    $Product_ID = $_POST['id'];
    $sql = "SELECT * FROM products where Product_ID = $Product_ID";
    $result = mysqli_query($con, $sql);
    $row = mysqli_fetch_assoc($result);
    ?>
<form method="post" action="modify.php" enctype="multipart/form-data">
    <input type="hidden" name="id" value='<?php echo $Product_ID;?>'/>
    <label>Product Name: </label><input type="text" name="productName" value="<?php echo $row['Product_name'];?>"/><br>
    <label>Product Description: </label><textarea rows="7" cols="100" name="productDescription"> <?php echo $row['Product_description'];?></textarea><br>
    <label>Product YouTube Link: </label><input type="text" name="productLink" value="<?php echo $row['Product_video_link'];?>"/><br>
    <label>Product Price: </label><input type="text" name="productPrice" value="<?php echo $row['Product_price'];?>"/><br>
    <label>Product Image:<input type="file" name="file" /><br>
    <input type="submit" value="Update" name="btnUpdate"/>
    <input type="submit" value="Cancel" name="btnCancelUpdate"/>
</form> 
<?php
}