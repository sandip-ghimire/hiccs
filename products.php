<?php
if(!isset($_SESSION)){
    session_start();
}
?>
<html>
  <head>
    <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="./images/hiccsicon.png"/>
        <title>HICCS</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<script src="./js/hiccs.js"></script>
  </head>
  <body>
        <?php
        include 'header.php';
        //here goes the body of the file
        include 'connection.php';
        echo '<table>';
          $sql = "SELECT * FROM products";
          $result = mysqli_query($con, $sql);
		  $i = 0;
          while ($rows = mysqli_fetch_assoc($result)) {
            ?>
            <div class="contentWrapper container" style="background-color:#eeeeee">
                <div class="row">      											
                    <?php 
                    $prodct_ID = $rows['Product_ID'];
                    if (0 == $i % 2) {
                        echo "<div class='col-4'><img src='Productimages/" . $rows['Product_image']. "' width = '90%' alt='" . $rows['Product_name'] . "'>" ?></div>
                        <div class="col-8">	<?php echo "<b><u>".$rows['Product_name'] ."</u></b>" . 
                           "<br>" . $rows['Product_description'] . 
                           "<br>Price: " . $rows['Product_price'] . 
                        "<br><a href = '" . $rows['Product_video_link']. "'>VIDEO</a>";
                        ?>
                            <form method="post" action="order.php">
                                <input type="hidden" name="productID" <?php echo "value = $prodct_ID";?>/>
                                <input type="submit" value="Order" name="btnOrder"/>
                            </form>
                            <?php
                        if(isset($_SESSION['username'])){
                        ?>
                            <form action="modify.php" method="post">
                                <input type="hidden" <?php echo "value = $prodct_ID" ?> name="id"/>
                                <input type="submit"  name="edit" value="Edit"/>
                                <input type="submit" name="delete" value="Delete"/> 
                            </form>
                            <?php
                        }
                    }
                    else {
                        echo "<div class='col-8 oddblock'><b><u>".$rows['Product_name'] ."</u></b>" . 
                           "<br>" . $rows['Product_description'] . 
                           "<br>Price: " . $rows['Product_price'] . 
                        "<br><a href = '" . $rows['Product_video_link']. "'>VIDEO</a>";
                        ?>
                            <form method="post" action="order.php">
                                <input type="hidden" name="productID" <?php echo "value = $prodct_ID"; ?>/>
                                <input type="submit" value="Order" name="btnOrder"/>
                            </form>
                            <?php
                        if(isset($_SESSION['username'])){ ?>
                            <form action="modify.php" method="post">
                                <input type="hidden" <?php echo "value = $prodct_ID" ?> name="id"/>
                                <input type="submit" name = "edit" value="Edit"/>
                                <input type="submit" name = "delete" value="Delete"/> 
                            </form>
                            <?php
                        }?>
                        </div>
                        <div class="col-4 oddblock"><?php echo "<img src='Productimages/" . $rows['Product_image']. "' width = '90%' alt='" . $rows['Product_name'] . "'>";	
                        
                    }?>
                        </div>
                        <?php
                        $i++;
        }
            echo '</div>';
   
        include 'footer.php';
        ?>	

            <script>
                    $('.nav-item').removeClass('active');
                    $('.products').addClass('active');
       </script>
  </body>
</html>
