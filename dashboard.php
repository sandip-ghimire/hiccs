<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!isset($_SESSION)) 
    session_start();
if(!isset($_SESSION['username'])){
    header("Location: admin_login.php");
}
include 'connection.php';
if(isset($_POST['btnAdd'])){
    $image_name = $_FILES['file']['name'];
    $db_image_name = time() . $image_name;//Store this name in DB
    $image_source = $_FILES['file']['tmp_name'];
    $image_destination = "ProductImages/" . $db_image_name;
    $prodct_name = mysqli_real_escape_string($con,$_POST['name']);
    $prodct_description = mysqli_real_escape_string($con,$_POST['description']);
    $prodct_link = mysqli_real_escape_string($con,$_POST['link']);
     if($image_name == NULL || $prodct_name == NULL || $prodct_description == NULL || $image_name == NULL ){
            echo "fill all fields";
            include 'add_products.html';
            exit;
        }
    if(move_uploaded_file($image_source, $image_destination)){
        //on sucess
        $product_price = (int)$_POST['price'];
        $category_ID = (int)$_POST['categories'];
        if($product_price != null){
            $sql = "INSERT INTO products(Product_name, Product_image, Product_description,Product_price, Product_video_link, category_ID) VALUES('$prodct_name', '$db_image_name', '$prodct_description',$product_price,'$prodct_link',$category_ID)";
        }else{
            $sql = "INSERT INTO products(Product_name, Product_image, Product_description,Product_price, Product_video_link, category_ID) VALUES('$prodct_name', '$db_image_name', '$prodct_description', null,'$prodct_link',$category_ID)";    
        }
         $result = mysqli_query($con, $sql);
         if($result == FALSE){
             echo 'Error inserting into database now';
         }else{
             echo"Succesfully inserted.";
         }
    }
}else if(isset($_GET['orderedProduct'])){
     include 'dashboardLeftTemplate.php';
     $sqlSelect = "SELECT
                        C.customer_fname AS fname,
                        C.customer_lname AS lname,
                        C.customer_email AS email,
                        CO.comment AS comment,
                        CO.Product_ID as ProductID,
                        P.Product_name AS ProductName
                    FROM customers  C
                    JOIN customers_order CO ON CO.customer_username = C.customer_username
                    JOIN products P ON CO.Product_ID = P.Product_ID";
     $result1 = mysqli_query($con, $sqlSelect);
     
?>
<table border="1"> 
    <tr> 
        <th>Product Number</th>
        <th>Ordered Product Name</th> 
        <th>Customer Name</th> 
        <th>Customer Email</th> 
        <th>Comment</th>
        <th>Mark</th> 
    </tr> 
    <?php
      $i = 1;
    while ($rows = mysqli_fetch_assoc($result1)) {
        
          ?>
    <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $rows['ProductName'];?></td>
        <td><?php echo $rows['fname'] . " " . $rows['lname'];?></td>
        <td><?php echo $rows['email'];?></;?></td>
        <td><?php echo $rows['comment'];?></td>
        <td>
            <form action="dashboard.php?serveOrder" method="get"> 
                <input type="hidden" <?php "value = ". $rows['ProductID'] ;?> name="ID"/>
                <input type="submit" value="Mark as Served" name="serve"/>
            </form>
        </td>
    </tr>
         <?php
         $i +=1;
    }
    ?>
</table>

<?php
}else{
    include 'add_products.html';
}


?>