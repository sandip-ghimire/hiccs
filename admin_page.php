<?php
session_start();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="./images/hiccsicon.png"/>
		<title>HICCS</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<script src="./js/hiccs.js"></script>
  </head>
  <body>	
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="index.html"><img src="images/hiccslogo.png" alt="HICCS" height="60" width="60"><b><span style="color: #9cc5fe;weight:bold">&nbsp;HICCS</span></b> Global</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blogs.html">Blogs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="add_products.php">Add Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contactus.html">Contact US</a>
                    </li>
                </ul>
            </div>
        </nav>
       <h1>Login Succeed. </h1>
       <!Same context as index.html >
        <footer class="footer mt-auto py-3">
            <div class="container">
                <a href="https://www.facebook.com/visitnatureandyou/"><img src="images/facebook.png" alt="facebook" height="20" width="20"></a>
                <a href="https://www.instagram.com/visitnatureandyou/"><img src="images/instagram.png" alt="instagram" height="20" width="20"></a>
                <a href="http://www.linkedin.com/company/nature-&-you/"><img src="images/linkedin.png" alt="linkedin" height="20" width="20"></a>
            </div>
        </footer>
  </body>
</html>