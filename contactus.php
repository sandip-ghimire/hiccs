<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
$sent=0;
if(isset($_POST['btnSend'])){

// Load Composer's autoloader
require 'vendor/autoload.php';
// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);
try {
   //Server settings
    //$mail->SMTPDebug = 2;                                       // Enable verbose debug output
    $mail->isSMTP();                                            // Set mailer to use SMTP
    $mail->Host       = 'mail.hiccs.fi;';  // Specify main and backup SMTP servers
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'abryee@hiccs.fi';                     // SMTP username
    $mail->Password   = 'LETSTRYTHIS';                               // SMTP password
    $mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
    $mail->Port       = 465;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('abryee@hiccs.fi', 'Abrhalei');
    $mail->addAddress('admin@hiccs.fi', 'Joe User');     // Add a recipient
    $mail->addAddress('abryeemessi@gmail.com');               // Name is optional
//    $mail->addReplyTo('info@example.com', 'Information');
//    $mail->addCC('cc@example.com');
//    $mail->addBCC('bcc@example.com');

//    // Attachments
//    $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//    $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'HICCS Website Email (From Customers)';
    $mail->Body    = '<h5> Customer Full Name: </h5><h7>'. $_POST['fullname'] .'</h7><br>'
            . '<h5> Customer Email Adress: </h5><h7>'. $_POST['email'] .'</h7><br>'
            . '<h5>Email Content: </h5><h7>'. $_POST['body']. '</h7>';
    $mail->AltBody ='Email : '. $_POST['email'] . 'Email Content: ' . $_POST['body'];

    $mail->send();
    $sent=1;
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    $sent=0;
}   
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="./images/hiccsicon.png"/>
        <title>HICCS</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<script src="./js/hiccs.js"></script>
	
  </head>
  <body>
        <!--  Header -->
       <?php 
       include 'header.php';
       ?>
       <!-- Body  -->
	   <div class="contentWrapper container">
		<div class="row"> 
		<div class="col-6 col-center"> 
			<div class="contactuscontent" style="padding:2em">			
       
           <h3 class='center' >HICCS</h3>
           <p>
               We truly care about our customers and their health and wellbeing, 
               which is why we work on diversifying our service offering to support 
               people across Europe. Feel free to contact us, we are keen to help and 
               work with people, as well as organisations or state.
           </p>
		   </div>
		   
		</div>
		   
		<div class="col-6 col-center">
                    <form action="contactus.php" method="post" class='contactusform'>
            <div class="form-group">
                <label >Name</label><br>
                <input  aria-describedby="name" name="fullname" class="form-control" placeholder="Enter Name"><br><br>
                <label >Email address</label><br>
                <input type="email" name="email" aria-describedby="emailHelp" class="form-control" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small><br>
                <label >Message</label><br>
                <textarea  name="body" class="form-control">
                    </textarea>
            </div>
            <div class="form-group form-check">
                <?php if($sent == 1) {echo 'Message sent!';}?>
                <button type="submit" name="btnSend" class="btn btn-info">Send</button>
            </div>
        </form>
		</div>
    
	
	</div>
	<hr>
	</div>
    <?php
      include 'footer.php';
    ?>
	
	<script>		
			$('.nav-item').removeClass('active');
			$('.contactus').addClass('active');		
	   </script>
  </body>
</html>