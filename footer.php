
<footer class="footer mt-auto py-3">
    <div class="container">
        <a href="https://www.facebook.com/visitnatureandyou/"><img src="images/facebook.png" alt="facebook" height="20" width="20"></a>
        <a href="https://www.instagram.com/visitnatureandyou/"><img src="images/instagram.png" alt="instagram" height="20" width="20"></a>
        <a href="http://www.linkedin.com/company/nature-&-you/"><img src="images/linkedin.png" alt="linkedin" height="20" width="20"></a>
    </div>
</footer>