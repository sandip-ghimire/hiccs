
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php"><img src="images/hiccslogo.png" alt="HICCS" height="60" width="60"><b><span style="color: #9cc5fe;weight:bold">&nbsp;HICCS</span></b> Global</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item home active">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item products">
                <a class="nav-link" href="products.php">Products and Services</a>
            </li>
            <li class="nav-item blogs">
                <a class="nav-link" href="blogs.php">Evolve</a>
            </li>
            <li class="nav-item blogs">
                <a class="nav-link" href="blogs.php">Orbit</a>
            </li>
            <li class="nav-item blogs">
                <a class="nav-link" href="blogs.php">Fusion-Ops</a>
            </li>
            <li class="nav-item contactus">
                <a class="nav-link" href="contactus.php">Contact Us</a>
            </li>
        </ul>
    </div>
</nav>
