<!DOCTYPE html>
<html>

  <head>
  
    <meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="./images/hiccsicon.png"/>
		<title>HICCS</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<script src="./js/hiccs.js"></script>
	
  </head>
  
  <body>
       <!--  Header -->
       <?php
       include 'header.php';
       ?>
       <!-- Body  -->
        <div class="container divcontainer py-2" >
		    <h3 class='center' style="margin-bottom:2%;margin-top:0%;font-weight:bold">HICCS Global Community</h3>
			
            <div class="row" style="background-color: #f6f5f559">
                <div class="col-12">
				  <div class='wrapbtn'>
                    <div class="blog-text" style="margin-top:0%">
                        
                        <p class='center text-justify px-4'>
                            We believe that community thinking is an important thing in society. 
                            People, businesses and society can, and should, work together under 
                            an umbrella of respect and shared values, well at least this is what 
                            we aim to achieve. Anyone in our community should be given tools to 
                            enjoy and develop their life in a direction that makes them happy and 
                            content. 
                        </p>
                        <p class='center text-justify px-4'>
                            We will look to arrange community specific events when the community 
                            has grown to a reputable size, and we hope this will help inspire the 
                            community to help share the message of 'Creating Happiness'.
                        </p>
                        <p class='center text-justify px-4'>
                            There will be no membership fees, but we will remove people, if necessary, 
                            who do not represent or respect our values.
                        </p>
                    </div>
                    <div class='blog-btn'>
                        <button type="button" class="btn btn-info btn-lg">Login</button>
                        <button type="button" class="btn btn-info btn-lg">Register</button>
                    </div>
					</div>
					
                    <h3 class='center' style="margin-bottom:3%;font-weight:bold">Blog</h3>
                    <div class='blog-tiles'>
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 col-center">
                            <div class="hovereffect">
                                <img class="img-responsive" src="./images/article1.jpg" alt="article-1" width="100%" height="100%">
                                <div class="overlay">
                                    <h2>en-Luonto ja universumi keskeisessä asemassa...</h2>
                                    <a class="info" href="https://www.mydvg.eu/l/en-luonto-ja-universumi-keskeisessa-asemassa/">View</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 col-center">
                            <div class="hovereffect">
                                <img class="img-responsive" src="./images/article2.jpg" alt="article-2" width="100%" height="100%">
                                <div class="overlay">
                                    <h2>en-Mistä kaikki alkoi...</h2>
                                    <a class="info" href="https://www.mydvg.eu/l/en-mista-kaikki-alkoi/">View</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 col-center">
                            <div class="hovereffect">
                                <img class="img-responsive" src="./images/article3.jpg" alt="article-3" width="100%" height="100%">
                                <div class="overlay">
                                <h2>en-Laadulla on suuri merkitys...</h2>
                                <a class="info" href="https://www.mydvg.eu/l/en-laadulla-on-suuri-merkitys/">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>   
            </div>
        </div>
        <!-- Footer -->
        <?php
        include 'footer.php';
       ?>
	   <script>
		
			$('.nav-item').removeClass('active');
			$('.blogs').addClass('active');
		
	   </script>
  </body>
</html>