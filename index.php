<!DOCTYPE html>
<?php
session_start();
?>
<html>
  <head>
    <meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="./images/hiccsicon.png"/>
		<title>HICCS</title>
    

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Chettan|PT+Sans|Raleway:500|Ubuntu|ZCOOL+XiaoWei" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<script src="./js/hiccs.js"></script>
	
  </head>
  <body>
	<?php
        include 'header.php';
    ?>
        <!-- here goes the body of the file -->
    <section id="header" class="slider">
    	<div class="slider-overlay">
    		<div id="headerSec" class="container">
				<h1 class="h_title"><span>HICCS</span>GLOBAL</h1>
				<!-- <h3 class="h_concept">Service Concept</h3> -->	
				<p class="h_subtitle">'Learn, Evolve, Innovate, Build Awareness'</p>
				<p class="h_desc">Home of next gen impact learning, knowledge share, innovative thinking, continuous development and evolving leadership. Our services and solutions are targeted and tailored to suite both private and public sector providers. HICCS is also the go to provider for specialist, sustainable staffing and recruitment solutions. Our ORBIT personal development journey is designed to improve and evolve candidate and employer matching accuracy.</p>    		
	    	</div>
    	</div>    	
    </section>
    <section class="content">
    	<div class="service ser-course">
    		<div id="courses" class="container ser_category">
	    		<div class="row">
	    			<div class="col-md-5">
	    				<div class="ser_img">
	    					<img src="images/home/HICCS_Coaching_ENG_01.png">
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="ser_desc">
	    					<h4>Course Offering</h4>
	    					<p class="ser_concept">'Learn, Evolve, Innovate, Build Awareness'</p>
	    					<p>The HICCS courses and learning programs are designed to support management, governance and business efficiency and effectiveness.  We bring both system and human capital solutions targeted at protecting productivity, affordability and contingency, all key future sustainability factors to consider.</p>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    	<div id="courseList" class="ser_list">
	    		<div class="ser_list_overlay">
		    		<div class="row">
			    		<div class="col-md-3">
			    			<div class="ser_item">
			    				<img class="featured_img" src="images/home/LLP_Stressi_Paineenhallinta.jpg">
				    			<p class="item_label">Stressi ja Paineenhallinta Valmennus
				    				<span class="duration">40 min</span>
				    			</p>
				    			<p class="item_price"> 30.00 € </p>
			    			</div>
			    		</div>
			    		<div class="col-md-3">
			    			<div class="ser_item">
			    				<img class="featured_img" src="images/home/LLP_Stressi_Paineenhallinta.jpg">
				    			<p class="item_label">Stressi ja Paineenhallinta Valmennus
				    				<span class="duration">40 min</span>
				    			</p>
				    			<p class="item_price"> 30.00 € </p>
			    			</div>
			    		</div>
			    		<div class="col-md-3">
			    			<div class="ser_item">
			    				<img class="featured_img" src="images/home/LLP_Stressi_Paineenhallinta.jpg">
				    			<p class="item_label">Stressi ja Paineenhallinta Valmennus
				    				<span class="duration">40 min</span>
				    			</p>
				    			<p class="item_price"> 30.00 € </p>
			    			</div>
			    		</div>
			    		<div class="col-md-3">
			    			<div class="ser_item">
			    				<img class="featured_img" src="images/home/LLP_Stressi_Paineenhallinta.jpg">
				    			<p class="item_label">Stressi ja Paineenhallinta Valmennus
				    				<span class="duration">40 min</span>
				    			</p>
				    			<p class="item_price"> 30.00 € </p>
			    			</div>
			    		</div>
		    		</div>
		    		<div class="row">
				    	<div class="btn_products">
				    		<button class="btn_all_products">
				    			<a href="products.php">All Products</a>
				    		</button>
				    	</div>
		    		</div>
		    	</div>
	    	</div>
    	</div>

    	<div class="service">
    		<div id="talentHire" class="container ser_category">
	    		<div class="row">
	    			<div class="col-md-5">
	    				<div class="ser_img">
	    					<img src="images/home/HICCS_Talent_Hire_ENG_01.png">
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="ser_desc">
	    					<h4>Talent and Solution Hire</h4>
	    					<p class="ser_concept">'Learn, Evolve, Innovate, Build Awareness'</p>
	    					<p>Technology and IT overall has a major role to play in the evolution of human life, and we see ourselves as a sustainable solution provider for both enterprise and people. Our organisation has the full capability to manage project life-cycle, from project strategy and design to technical delivery. Get in touch for tailored / semi-tailored solutions or trial and purchase our ready made tech-solutions direct.</p>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    	<div id="talentHireList" class="ser_list">
	    		<div class="ser_list_overlay">
		    		<div class="row">
			    		<div class="col-md-3">
			    			<div class="ser_item">
			    				<img class="featured_img" src="images/home/LLP_Stressi_Paineenhallinta.jpg">
				    			<p class="item_label">Stressi ja Paineenhallinta Valmennus
				    				<span class="duration">40 min</span>
				    			</p>
				    			<p class="item_price"> 30.00 € </p>
			    			</div>
			    		</div>
		    		</div>
		    		<div class="row">
				    	<div class="btn_products">
				    		<button class="btn_all_products">
				    			<a href="products.php">All Products</a>
				    		</button>
				    	</div>
		    		</div>
		    	</div>
	    	</div>
    	</div>

    	<div class="service">
    		<div id="events" class="container ser_category">
	    		<div class="row">
	    			<div class="col-md-5">
	    				<div class="ser_img">
	    					<img src="images/home/HICCS_Events_ENG_01.png">
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="ser_desc">
	    					<h4>Events World</h4>
	    					<p class="ser_concept">'Learn, Evolve, Innovate, Build Awareness'</p>
	    					<p>We bring you a new age events concept, no more dull and boring speakers (especially a problem in Finland), who lack charisma and love to listen to their own voice. Our events are designed to engage people and create genuine value added networking opportunities. Our interactive panel model will ensure there are always value added points to take away and it forces constructive dialogue. Some events will also be accompanied with all evening dinner.</p>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    	<div id="eventList" class="ser_list">
	    		<div class="ser_list_overlay">
		    		<div class="row">
			    		<div class="col-md-3">
			    			<div class="ser_item">
			    				<img class="featured_img" src="images/home/LLP_Stressi_Paineenhallinta.jpg">
				    			<p class="item_label">Stressi ja Paineenhallinta Valmennus
				    				<span class="duration">40 min</span>
				    			</p>
				    			<p class="item_price"> 30.00 € </p>
			    			</div>
			    		</div>
		    		</div>
		    		<div class="row">
				    	<div class="btn_products">
				    		<button class="btn_all_products">
				    			<a href="products.php">All Products</a>
				    		</button>
				    	</div>
		    		</div>
		    	</div>
	    	</div>
    	</div>

    	<div class="service">
    		<div id="orbit" class="container ser_category">
	    		<div class="row">
	    			<div class="col-md-5">
	    				<div class="ser_img">
	    					<img src="images/home/HICCS_ORBIT_FIN_01.png">
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="ser_desc">
	    					<h4>ORBIT</h4>
	    					<p class="ser_concept">'Learn, Evolve, Innovate, Build Awareness'</p>
	    					<p>ORBIT is our soft skills personal development journey based on our 6-20 Revolution Competency Framework. The ORBIT tool-pack allows you to review your own competency dynamic, which in turn can be matched to employers. ORBIT has very high candidate to employer matching impact rate. If you want a successful career you should look at ORBIT as your ticket for getting there.</p>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    	<div id="orbitList" class="ser_list">
	    		<div class="ser_list_overlay">
		    		<div class="row">
			    		<div class="col-md-3">
			    			<div class="ser_item">
			    				<img class="featured_img" src="images/home/LLP_Stressi_Paineenhallinta.jpg">
				    			<p class="item_label">Stressi ja Paineenhallinta Valmennus
				    				<span class="duration">40 min</span>
				    			</p>
				    			<p class="item_price"> 30.00 € </p>
			    			</div>
			    		</div>
		    		</div>
		    		<div class="row">
				    	<div class="btn_products">
				    		<button class="btn_all_products">
				    			<a href="products.php">All Products</a>
				    		</button>
				    	</div>
		    		</div>
		    	</div>
	    	</div>
    	</div>

    	<div class="service">
    		<div id="fusionOps" class="container ser_category">
	    		<div class="row">
	    			<div class="col-md-5">
	    				<div class="ser_img">
	    					<img src="images/home/HICCS_Fusion-Ops_ENG_01.png">
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="ser_desc">
	    					<h4>Fusion-Ops</h4>
	    					<p class="ser_concept">'Learn, Evolve, Innovate, Build Awareness'</p>
	    					<p>The future requires us to evolve, change our perspective, and especially optimise how we do things. Fusion-Ops is a holistic business management model aimed at breaking down and removing waste, both machine an human, in business including obsolete leadership positions. The aim is to bring more machine and self-learning, as well as artificial intelligence into the business world, which will pave the way for a brighter future for all.</p>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    	<div id="fusionOpsList" class="ser_list">
	    		<div class="ser_list_overlay">
		    		<div class="row">
			    		<div class="col-md-3">
			    			<div class="ser_item">
			    				<img class="featured_img" src="images/home/LLP_Stressi_Paineenhallinta.jpg">
				    			<p class="item_label">Stressi ja Paineenhallinta Valmennus
				    				<span class="duration">40 min</span>
				    			</p>
				    			<p class="item_price"> 30.00 € </p>
			    			</div>
			    		</div>
		    		</div>
		    		<div class="row">
				    	<div class="btn_products">
				    		<button class="btn_all_products">
				    			<a href="products.php">All Products</a>
				    		</button>
				    	</div>
		    		</div>
		    	</div>
	    	</div>
    	</div>
    </section>
    <section id="socialMedia"></section>
    <?php
        include 'footer.php';
    ?>	
		
		<script>
		
			$('.nav-item').removeClass('active');
			$('.home').addClass('active');
		
	   </script>
  </body>
</html>